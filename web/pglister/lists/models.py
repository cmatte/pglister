from django.db import models, connection
from django.forms import ValidationError
from django.contrib.auth.models import User
from django.core.validators import RegexValidator

from lib.baselib.lists import MailingList, ModerationLevels, SubscriptionPolicies
from lib.baselib.lists import CcPolicies, BccPolicies

import re

from .sizefield import SizeField


def ValidateRegexpList(val):
    for r in val.splitlines():
        if r == '':
            raise ValidationError("Empty regexp found")
        try:
            re.compile(r, re.MULTILINE)
        except Exception as e:
            raise ValidationError("Regexp '{0}' error: {1}".format(r, e))


def ValidateCleanMessageId(val):
    if val.startswith('<') or val.endswith('>'):
        raise ValidationError("Messageid should be written without <> around it")
    if ' ' in val:
        raise ValidationError("No spaces in messageids!")


def ValidateRegexp(val):
    if val == '':
        return ''

    try:
        re.compile(val)
    except Exception as e:
        raise ValidationError("Regexp '{0}' error: {1}".format(val, e))


class Subscriber(models.Model):
    user = models.OneToOneField(User, null=False, blank=False, primary_key=True, on_delete=models.CASCADE)
    eliminatecc = models.BooleanField(null=False, blank=False, default=False, help_text="Don't receive an extra copy of mails when listed in To or CC fields")

    def __str__(self):
        return "{0} - {1} {2} <{3}>".format(
            self.user.username,
            self.user.first_name,
            self.user.last_name,
            self.user.email,
        )

    @property
    def fullname(self):
        return "{0} {1}".format(self.user.first_name, self.user.last_name)

    class Meta:
        ordering = ('user__username', )


class SubscriberAddress(models.Model):
    subscriber = models.ForeignKey(Subscriber, null=True, blank=True, on_delete=models.CASCADE)
    email = models.EmailField(null=False, blank=False, unique=True)
    confirmed = models.BooleanField(null=False, blank=False, default=False)
    blocked = models.BooleanField(null=False, blank=False, default=False)
    # NOTE! This token is used in list headers to identify the address,
    # it is different from the SubscriberAddressToken which is used to
    # confirm new addresses (and is later removed).
    token = models.TextField(null=False, blank=False, unique=True)

    def __str__(self):
        return self.email

    @property
    def has_subscriptions(self):
        return self.listsubscription_set.exists()

    class Meta:
        ordering = ['email']


class SubscriberAddressToken(models.Model):
    # These tokens are used for confirmation of subscriber addresses.
    # NOTE! They are different from the token used on a subscriber
    # address when delivering email!
    subscriberaddress = models.OneToOneField(SubscriberAddress, null=False, blank=False, primary_key=True, on_delete=models.CASCADE)
    tokensent = models.DateTimeField(null=False, blank=False, auto_now_add=True)
    token = models.TextField(null=False, blank=False, unique=True)


class ArchiveServer(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    urlpattern = models.CharField(max_length=200, null=False, blank=False)
    mailurlpattern = models.CharField(max_length=200, null=False, blank=False)
    maildomain = models.CharField(max_length=200, null=False, blank=False)
    apikey = models.CharField(max_length=100, null=False, blank=True)

    def __str__(self):
        return self.name


class Domain(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    lists_helppage = models.URLField(max_length=100, null=False, blank=False)
    archiveservers = models.ManyToManyField(ArchiveServer, blank=True)

    def __str__(self):
        return self.name


class ListGroup(models.Model):
    groupname = models.CharField(max_length=100, null=False, blank=False)
    sortkey = models.IntegerField(null=False, default=10)
    subscriber_access = models.BooleanField(null=True, blank=False, default=False,
                                            help_text="Can subscribers get full access to the archives?")

    def __str__(self):
        return self.groupname

    class Meta:
        ordering = ('sortkey', )


class List(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False, validators=[RegexValidator(r"^[a-zA-Z0-9-]+$", "List name can only contain alphanumerical characters and hypens.")])
    shortdesc = models.TextField(null=False, blank=True)
    longdesc = models.TextField(null=False, blank=True)
    domain = models.ForeignKey(Domain, null=False, blank=False, on_delete=models.CASCADE)
    group = models.ForeignKey(ListGroup, null=False, blank=False, on_delete=models.CASCADE)

    subscription_policy = models.IntegerField(null=False, blank=False,
                                              choices=SubscriptionPolicies.choices)
    members_view_membership = models.BooleanField(null=False, blank=False, default=False,
                                                  help_text="Can members view the membership of the list?")
    moderation_level = models.IntegerField(null=False, blank=False,
                                           choices=ModerationLevels.choices)
    moderation_regex = models.TextField(null=False, blank=True, validators=[ValidateRegexpList, ])

    spamscore_threshold = models.DecimalField(null=True, blank=True,
                                              max_digits=5, decimal_places=1)

    maxsizemoderate = SizeField(null=True, blank=True, help_text='Larger than this will be moderated. Enter with KB, MB, GB as necessary. NULL means default.')
    maxsizedrop = SizeField(null=True, blank=True, help_text='Larger than this will be silently dropped. Enter with KB, MB, GB as necessary. NULL means default.')

    bcc_policy = models.IntegerField(null=False, blank=False, default=0, choices=BccPolicies.choices)
    cc_policy = models.IntegerField(null=False, blank=False, default=0, choices=CcPolicies.choices)

    whitelist_regex = models.TextField(null=False, blank=True, help_text='Regular expressions to match against sender addresses and allow posts from matching. One regex per line.', validators=[ValidateRegexpList, ])
    blocklist_regex = models.TextField(null=False, blank=True, help_text='Regular expressions to match against sender addresses and moderate posts from matching. One regex per line.', validators=[ValidateRegexpList, ])
    ignore_global_mod_regex = models.BooleanField(null=False, blank=False, default=False, help_text="Bypass global moderation regex matching for posts to this list")

    send_moderation_notices = models.BooleanField(null=False, blank=False, default=True, help_text="Send notices to authors of emails held for moderation)")
    reject_to_cc = models.EmailField(null=True, blank=True, help_text='Send moderation rejection notices to the address in the CC field if the incoming email was sent from this address (typically a noreply address)')
    disable_list_headers = models.BooleanField(null=False, blank=False, default=False, help_text="Disable setting list headers when forwarding email. This also disables DKIM list header enforcement.")

    notify_subscriptions = models.BooleanField(null=False, blank=False, default=False, help_text="Send notification email to moderators on subscriptions and unsubscriptions")

    archivedat = models.ForeignKey(ArchiveServer, null=True, blank=True, on_delete=models.CASCADE)

    moderators = models.ManyToManyField(Subscriber, blank=False, related_name='moderator_of_lists')
    subscribers = models.ManyToManyField(SubscriberAddress, blank=True, through='ListSubscription')

    tagged_delivery = models.BooleanField(null=False, blank=False, default=False, verbose_name='Use tags for delivery')
    tagkey = models.CharField(max_length=100, null=False, blank=True, verbose_name="Hash key for tags")
    taglistsource = models.URLField(max_length=100, null=False, blank=True, verbose_name="URL to list of tags")

    apikey_ro = models.CharField(max_length=100, null=False, blank=True, verbose_name="Read-only API key")
    apikey_rw = models.CharField(max_length=100, null=False, blank=True, verbose_name="Read-write API key")

    def __str__(self):
        return "{0}@{1}".format(self.name, self.domain.name)

    class Meta:
        ordering = ('domain__name', 'name')
        unique_together = (
            ('name', 'domain'),
        )

    @property
    def archive_url(self):
        return self.archivedat.urlpattern.replace('%', self.name)

    _wrapper = None

    @property
    def wrapper(self):
        # Get a wrapper to the non-django side of the system to access rules for
        # things like owner address.
        if self._wrapper is None:
            self._wrapper = MailingList.get_by_id(connection, self.id)
        return self._wrapper

    def owner_address(self):
        return self.wrapper.owner_address()

    def owner_name(self):
        return self.wrapper.owner_name()

    def moderator_notice_address(self):
        return self.wrapper.moderator_notice_address()

    def moderator_notice_name(self):
        return self.wrapper.moderator_notice_name()

    @property
    def subscription_policy_string(self):
        return next((c[1] for c in SubscriptionPolicies.choices if c[0] == self.subscription_policy))

    @property
    def moderation_level_string(self):
        return next((c[1] for c in ModerationLevels.choices if c[0] == self.moderation_level))

    @property
    def subcount(self):
        return ListSubscription.objects.filter(list=self, subscription_confirmed=True).count()


class ListAlias(models.Model):
    list = models.ForeignKey(List, null=False, blank=False, on_delete=models.CASCADE)
    alias = models.EmailField(max_length=100, null=False, blank=False, unique=True,
                              help_text="Enter the full email address of the alias")

    def __str__(self):
        return self.alias

    class Meta:
        verbose_name_plural = 'List aliases'


class ListTag(models.Model):
    list = models.ForeignKey(List, null=False, blank=False, on_delete=models.CASCADE)
    tag = models.CharField(max_length=50, null=False, blank=False)
    name = models.CharField(max_length=50, null=False, blank=False)
    description = models.CharField(max_length=200, null=False, blank=False)
    sortkey = models.IntegerField(null=False, blank=False, default=100)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['list', 'sortkey', 'tag']
        unique_together = (
            ('list', 'tag'),
        )


class ListSubscription(models.Model):
    list = models.ForeignKey(List, null=False, blank=False, on_delete=models.CASCADE)
    subscriber = models.ForeignKey(SubscriberAddress, null=False, blank=False, on_delete=models.CASCADE)

    subscription_confirmed = models.BooleanField(null=False, blank=False, default=True)
    nomail = models.BooleanField(null=False, blank=False, default=False)
    tags = models.ManyToManyField(ListTag, blank=True)

    class Meta:
        ordering = ['list', 'subscriber']
        unique_together = (
            ('list', 'subscriber',)
        )


class ListSubscriptionModerationToken(models.Model):
    subscription = models.OneToOneField(ListSubscription, null=False, blank=False, on_delete=models.CASCADE)
    tokensent = models.DateTimeField(null=False, blank=False)
    token = models.TextField(null=False, blank=False, unique=True)


class ListWhitelist(models.Model):
    list = models.ForeignKey(List, null=False, blank=False, on_delete=models.CASCADE)
    address = models.EmailField(null=False, blank=False)
    added_at = models.DateTimeField(null=False, blank=False, auto_now_add=True, db_index=True)

    class Meta:
        unique_together = (
            ('list', 'address'),
        )
        ordering = ('list', 'address')

    def __str__(self):
        return "{0} for {1}".format(self.address, self.list)


class GlobalWhitelist(models.Model):
    address = models.EmailField(null=False, blank=False, primary_key=True)

    def __str__(self):
        return self.address


class GlobalBlocklist(models.Model):
    address = models.CharField(max_length=254, null=False, blank=False, unique=True, validators=[ValidateRegexp], help_text="Regular expression to match")
    exclude = models.CharField(max_length=255, null=False, blank=True, validators=[ValidateRegexp], help_text="Regular expression to exclude from matching")
    usecount = models.IntegerField(null=False, blank=False, default=0)
    lastused = models.DateTimeField(null=True, blank=True, auto_now_add=True)

    def __str__(self):
        return self.address

    class Meta:
        ordering = ('address', )


class GlobalBlocklistMatch(models.Model):
    entry = models.ForeignKey(GlobalBlocklist, null=False, blank=False, on_delete=models.CASCADE)
    t = models.DateTimeField(null=False, blank=False, auto_now_add=True)
    messageid = models.CharField(max_length=1000, null=False, blank=False)

    class Meta:
        ordering = ('t', )


class NeverBlockRegexp(models.Model):
    regexp = models.CharField(max_length=255, null=False, blank=False, validators=[ValidateRegexp], help_text="Regular expression to always disallow blocklisting of")

    def __str__(self):
        return self.regexp


class GlobalModerationList(models.Model):
    expression = models.CharField(max_length=500, null=False, blank=False,
                                  validators=[ValidateRegexpList, ])
    moderate_sender = models.BooleanField(null=False, blank=False, default=True)
    moderate_subject = models.BooleanField(null=False, blank=False, default=True)
    moderate_body = models.BooleanField(null=False, blank=False, default=True)
    comments = models.TextField(null=False, blank=True)

    def __str__(self):
        return self.expression

    class Meta:
        ordering = ('expression', )


class MessageidModerationList(models.Model):
    messageid = models.CharField(max_length=1000, null=False, blank=False, unique=True,
                                 validators=[ValidateCleanMessageId, ])
    addedat = models.DateTimeField(null=False, blank=False, auto_now=True)

    def __str__(self):
        return self.messageid


class CannedRejectResponse(models.Model):
    title = models.CharField(max_length=20, null=False, blank=False, unique=True)
    content = models.TextField(null=False, blank=False)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('title', )


class CannedUnsubscriptionNotice(models.Model):
    title = models.CharField(max_length=30, null=False, blank=False, unique=True)
    syskey = models.CharField(max_length=16, null=True, blank=True, unique=True, help_text="Internal lookup key")
    sortkey = models.IntegerField(null=False, default=10)
    content = models.TextField(null=False, blank=False)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('sortkey', 'title', )

    @property
    def description(self):
        return self.content

    @classmethod
    def get_system(cls, key):
        return CannedUnsubscriptionNotice.objects.get(syskey=key)


class UnsubscribeLog(models.Model):
    list = models.ForeignKey(List, null=False, blank=False, on_delete=models.CASCADE)
    subscriber = models.ForeignKey(SubscriberAddress, null=False, blank=False, on_delete=models.CASCADE)
