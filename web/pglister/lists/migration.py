from django.db import transaction
from django.conf import settings

from lib.baselib.misc import generate_random_token

from pglister.listlog.util import log, Level
from .models import Subscriber, SubscriberAddress


@transaction.atomic
def handle_user_data(sender, user, userdata, **kwargs):
    # This shouldn't happen if we're not using pg auth, but just to be sure
    if not settings.USE_PG_COMMUNITY_AUTH:
        return

    # Make sure this user has a subscriber record
    subscriber, _ = Subscriber.objects.get_or_create(user=user)

    def _process_subscriberaddress(email):
        try:
            sa = SubscriberAddress.objects.get(email=email)
            if sa.subscriber_id is None:
                # This was an orphaned subscriberaddress -- so we can just attach it and
                # mark it as confirmed.
                sa.subscriber = subscriber
                sa.confirmed = True
                sa.save(update_fields=['subscriber', 'confirmed'])
                log(Level.INFO, user, "Associated existing subscriberaddress for {} with subscriber.".format(email))
            elif sa.subscriber_id != subscriber.user_id:
                # TROUBLE!
                # This subscriberaddress is already connected to a different account. This shouldn't be possible...
                # Log it and ignore (the log will trigger alerts)
                log(Level.ERROR, user, "Email address {} is connected to {}, but should be associated with {}".format(email, sa.subscriber, subscriber))

            # Else it is already the correct one, so nothing to see here.
        except SubscriberAddress.DoesNotExist:
            # Subscriberaddress did not exist, but it should, so create it
            SubscriberAddress(subscriber=subscriber, email=email, confirmed=True, token=generate_random_token()).save()
            log(Level.INFO, user, "Created new subscriberaddress for {}.".format(email))

    # Make sure each of the users email addresses has a subscriberaddress,
    # including the primary one.
    _process_subscriberaddress(user.email)
    for e in userdata['secondaryemails']:
        _process_subscriberaddress(e)

    # Delete any subscriberaddresses for email addresses that don't
    # exist anymore. We do this in a loop because we want it to be
    # easy to log the details, and it's not like it's going to b
    # *that* many in any realistic use-case.
    for sa in SubscriberAddress.objects.filter(subscriber=subscriber).exclude(email__in=userdata['secondaryemails'] + [user.email, ]):
        for ls in sa.listsubscription_set.all():
            log(Level.INFO, user, "Automatically unsubscribed {} from {} in preparation of removing subscriberaddress".format(sa.email, ls.list))
            ls.delete()
        log(Level.INFO, user, "Automatically removed subscriberaddress for {}".format(sa.email))
        sa.delete()
