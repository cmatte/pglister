from django.shortcuts import get_object_or_404, Http404
from django.http import HttpResponseRedirect
from django.views.generic import FormView, TemplateView, ListView
from django.contrib import messages
from django.db import connection, transaction
from django.core.validators import validate_email
from django.conf import settings

import requests

from lib.baselib.misc import generate_random_token

from .util import LoginRequired
from .util import SimpleWebResponse
from .util import complete_subscription
from pglister.listlog.util import log, Level
from pglister.util import exec_to_dict, exec_to_list
from pglister.eximintegration.util import maybe_refresh_exim_queue

from .models import List, ListWhitelist, Subscriber, SubscriberAddress, ListSubscription
from .models import MessageidModerationList, CannedUnsubscriptionNotice
from .forms import ManageSubscribersUnsubscribeForm

from .views_moderate import notify_unsubscribed_user, log_unsubscription


class ManageList(LoginRequired, TemplateView):
    template_name = 'managelist.html'

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_superuser:
            self.list = get_object_or_404(List.objects.select_related('domain'), id=kwargs['id'])
        else:
            self.list = get_object_or_404(List.objects.select_related('domain'), id=kwargs['id'], moderators__user=self.request.user)
        return super(ManageList, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ManageList, self).get_context_data(**kwargs)

        context['list'] = self.list
        context['whitelist'] = ListWhitelist.objects.filter(list=self.list)
        if self.list.tagged_delivery:
            context['tagstats'] = exec_to_list("""
 SELECT name, count(*) FILTER (WHERE ls.id IS NOT NULL)
 FROM lists_listtag t
 LEFT JOIN lists_listsubscription_tags lt ON t.id=lt.listtag_id
 LEFT JOIN lists_listsubscription ls ON ls.id=lt.listsubscription_id
 WHERE t.list_id=%(listid)s
 GROUP BY name
UNION ALL
 SELECT 'No tags/all mail', count(*)
 FROM lists_listsubscription ls2
 WHERE ls2.list_id=%(listid)s
  AND NOT EXISTS (SELECT 1 FROM lists_listsubscription_tags lt2 WHERE lt2.listsubscription_id=ls2.id)
ORDER BY 2 DESC""", {
                'listid': self.list.id,
            })

        return context

    @transaction.atomic
    def post(self, request, id):
        if request.POST['op'] == 'sub':
            return self._post_subscribe(request)
        if request.POST['op'] == 'wl':
            return self._post_whitelist(request)
        else:
            raise Exception("Not implemented")

    def _post_subscribe(self, request):
        m = request.POST['subscriberemail'].lower()
        try:
            validate_email(m)
        except Exception:
            messages.error(request, 'Email address {0} has invalid format.'.format(m))
            return HttpResponseRedirect('.')

        # If a subscriber exists, pick it up. If not, create a new one and set if to confirmed
        # so it can immediately start receiving emails. It will not be connected to an account,
        # but that will be done whenever the user connects.
        s, created = SubscriberAddress.objects.get_or_create(email=m, defaults={'confirmed': True, 'token': generate_random_token()})
        s.save()

        # We bypass moderation as this request by definition already came from a moderator
        ls, created = ListSubscription.objects.get_or_create(list=self.list, subscriber=s)
        if created:
            log(Level.INFO, request.user, "Subscribed address {0} to {1}.".format(m, self.list))
            messages.info(request, "Subscribed address {0} to {1}.".format(m, self.list))

            complete_subscription(request.user, self.list, m)
        else:
            messages.warning(request, "Address {0} already subscribed.".format(m))
        return HttpResponseRedirect('.')

    def _post_whitelist(self, request):
        if request.POST.get('addemail', None):
            m = request.POST['addemail'].lower()
            try:
                validate_email(m)
            except Exception:
                messages.error(request, 'Email address {0} has invalid format.'.format(m))
                return HttpResponseRedirect('.')
            (lw, created) = ListWhitelist.objects.get_or_create(list=self.list, address=m)
            if created:
                messages.info(request, "Added {0} to whitelist".format(m))
                log(Level.INFO, request.user, "Added {0} to whitelist for {1}".format(m, self.list))
            else:
                messages.warning(request, "{0} already on whitelist".format(m))

        for k in request.POST.getlist('delwl'):
            wl = ListWhitelist.objects.get(pk=k, list=self.list)
            messages.info(request, "Deleted {0} from whitelist".format(wl.address))
            log(Level.INFO, request.user, "Deleted {0} from whitelist for {1}".format(wl.address, self.list))
            wl.delete()
        return HttpResponseRedirect('.')


class ManageSubscribers(LoginRequired, ListView):
    model = ListSubscription
    paginate_by = 100
    template_name = 'managelist_subscribers.html'

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_superuser:
            self.list = get_object_or_404(List.objects.select_related('domain'), id=kwargs['id'])
        else:
            self.list = get_object_or_404(List.objects.select_related('domain'), id=kwargs['id'], moderators__user=self.request.user)
        return super(ManageSubscribers, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ManageSubscribers, self).get_context_data(**kwargs)
        context['list'] = self.list

        if 'search' in self.request.GET:
            context['baseurl'] = '?search={0}&'.format(self.request.GET['search'])
        else:
            context['baseurl'] = '?'
        return context

    def get_queryset(self):
        qsbase = super(ManageSubscribers, self).get_queryset().filter(list=self.list).select_related('subscriber')
        if self.list.tagged_delivery:
            qsbase = qsbase.prefetch_related('tags')
        if self.request.GET.get('search', None):
            return qsbase.filter(subscriber__email__icontains=self.request.GET['search']).order_by('subscriber__email')
        else:
            return qsbase.order_by('subscriber__email')


class ManageGlobalSubscribers(LoginRequired, ListView):
    model = ListSubscription
    paginate_by = 100
    template_name = 'managelist_global_subscribers.html'

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_superuser:
            raise Http404("Nope")
        return super(ManageGlobalSubscribers, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ManageGlobalSubscribers, self).get_context_data(**kwargs)

        context['search'] = self.request.GET['search']
        if 'search' in self.request.GET:
            context['baseurl'] = '?search={0}&'.format(self.request.GET['search'])
        else:
            context['baseurl'] = '?'
        context['unsubnotices'] = CannedUnsubscriptionNotice.objects.all()
        return context

    def get_queryset(self):
        if self.request.GET.get('search', None):
            return super(ManageGlobalSubscribers, self).get_queryset().filter(subscriber__email__icontains=self.request.GET['search']).select_related('subscriber', 'list', 'list__domain').order_by('subscriber__email')
        else:
            # No hits!
            return []

    @transaction.atomic
    def post(self, request):
        ids = [int(k.split('_')[1]) for k, v in request.POST.items() if k.startswith('s_') and v == '1']
        if not ids:
            messages.error(request, "No addresses selected for unsubscription!")
            return HttpResponseRedirect("?search={0}".format(request.POST['search']))

        for id in ids:
            ls = get_object_or_404(ListSubscription, id=id)
            notify_unsubscribed_user(ls, request.POST.get('unsubnote'))
            log(Level.INFO, request.user, "Unsubscribed user {0} from {1}.".format(ls.subscriber.email, ls.list))
            log_unsubscription(ls)
            ls.delete()

        messages.info(request, "Unsubscribed {0} users.".format(len(ids)))
        return HttpResponseRedirect("../../../?global=1")


class ManageThreadBlocklist(LoginRequired, ListView):
    model = MessageidModerationList
    paginate_by = 100
    template_name = 'managelist_thread_blocklist.html'

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_superuser:
            raise Http404("Nope")
        return super(ManageThreadBlocklist, self).dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(ManageThreadBlocklist, self).get_context_data(*args, **kwargs)
        context['has_archivesapi'] = len(settings.ARCHIVES_API_SERVERS) > 0
        return context

    def archives_messageid_list(self):
        for s in settings.ARCHIVES_API_SERVERS:
            try:
                r = requests.get(s, timeout=5)
                if r.status_code == 200:
                    for m in r.json():
                        yield m
            except Exception:
                pass

    @transaction.atomic
    def post(self, request):
        if request.POST['submit'] == "Remove":
            id = int(request.POST['delid'])
            item = get_object_or_404(MessageidModerationList, pk=id)
            m = item.messageid
            log(Level.INFO, request.user, "Removed {0} from messageid blocklist".format(m), m)
            item.delete()
            messages.info(request, "{0} removed from blocklist.".format(m))
            return HttpResponseRedirect(".")
        elif request.POST['submit'] == "Add to blocklist":
            messageid = request.POST['messageid'].lstrip('<').rstrip('>')
            if MessageidModerationList.objects.filter(messageid=messageid).exists():
                messages.info(request, "{0} is already on the blocklist.".format(messageid))
            else:
                MessageidModerationList(messageid=messageid).save()
                log(Level.INFO, request.user, "Added {0} to messageid blocklist".format(messageid), messageid)
                messages.info(request, "{0} added to blocklist.".format(messageid))
            return HttpResponseRedirect(".")
        raise Exception("Unknown submit")


class ManageSubscribersUnsubscribe(LoginRequired, FormView):
    template_name = 'baseform.html'
    success_url = '../..'
    form_class = ManageSubscribersUnsubscribeForm
    savebutton = 'Unsubscribe'
    cancelurl = '../'

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_superuser:
            self.list = get_object_or_404(List, id=kwargs['id'])
        else:
            self.list = get_object_or_404(List, id=kwargs['id'], moderators__user=self.request.user)
        return super(ManageSubscribersUnsubscribe, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        s = ListSubscription.objects.get(pk=self.request.POST['sid'])
        if s.list != self.list:
            raise Exception("Internal consistency error")

        kw = super(ManageSubscribersUnsubscribe, self).get_form_kwargs()
        kw['prompt'] = 'Please confirm that you want to unsubscribe {0} from {1}.'.format(s.subscriber, self.list)
        return kw

    @transaction.atomic
    def form_valid(self, form):
        s = ListSubscription.objects.get(pk=form.cleaned_data['sid'])

        notify_unsubscribed_user(s, form.cleaned_data['notification'])
        log(Level.INFO, self.request.user, "Unsubscribed {0} from {1}".format(s.subscriber, s.list))
        messages.info(self.request, "Unsubscribed {0} from {1}".format(s.subscriber, s.list))
        log_unsubscription(s)
        s.delete()

        return super(ManageSubscribersUnsubscribe, self).form_valid(form)


class ManageBounces(LoginRequired, TemplateView):
    template_name = 'bounces.html'

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_superuser:
            self.list = get_object_or_404(List.objects.select_related('domain'), id=kwargs['listid'])
        else:
            self.list = get_object_or_404(List.objects.select_related('domain'), id=kwargs['listid'], moderators__user=self.request.user)

        self.sub = get_object_or_404(SubscriberAddress, pk=kwargs['sub'], listsubscription__list=self.list)

        maybe_refresh_exim_queue()

        return super(ManageBounces, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ManageBounces, self).get_context_data(**kwargs)
        context['list'] = self.list
        context['sub'] = self.sub

        qs = """WITH t AS (
 SELECT DISTINCT ON (deliveryid) id, deliveryid, dt, transient, header, message
 FROM matched_bounces
 WHERE list=%(list)s AND subscriber=%(sub)s AND dt>now()-'1 month'::interval
 ORDER BY deliveryid, dt DESC
)
SELECT t.id,
       t.deliveryid,
       t.dt,
       trim(both '<>' from c.messageid) AS messageid,
       c.completedts,
       t.transient,
       string_to_array(t.header, chr(10)) AS header,
       string_to_array(t.message, chr(10)) AS message,
       c.tags,
       {eximfields}
FROM outgoing_completed c
LEFT JOIN t ON c.id=t.deliveryid
WHERE c.completedts>now()-'1 month'::interval
  AND c.sendinglist=(SELECT address FROM mailinglists WHERE id=%(list)s)
ORDER BY completedts DESC""".format(
            eximfields='EXISTS (SELECT 1 FROM eximintegration.queue q WHERE q.sender=verp_sender(c.sendinglist, %(sub)s, c.id)) AS ineximqueue' if settings.EXIM_INTEGRATION else 'NULL AS ineximqueue',
        )

        context['bounces'] = exec_to_dict(qs, {
            'list': self.list.pk,
            'sub': self.sub.pk,
        })

        context['unsubnotices'] = CannedUnsubscriptionNotice.objects.all()

        if self.request.user.is_superuser:
            # Find bounce status for this user on other lists
            context['otherlists'] = exec_to_dict(
                """WITH bounced AS (
 SELECT
  list,
  count(DISTINCT deliveryid) AS bounces,
  count(DISTINCT deliveryid) FILTER (WHERE dt>now()-'7 days'::interval) AS bounces_last_7
 FROM matched_bounces m
 INNER JOIN outgoing_completed c ON (c.id=m.deliveryid)
 WHERE
  m.subscriber=%(subid)s
  AND dt>now()-'1 month'::interval
  AND NOT transient GROUP BY list
), delivered AS (
 SELECT
  m.id AS listid,
  m.address,
  count(*) AS deliveries,
  count(*) FILTER (WHERE completedts>now()-'7 days'::interval) AS deliveries_last_7
 FROM outgoing_completed c
 INNER JOIN mailinglists m ON m.address=c.sendinglist
 WHERE completedts>now()-'1 month'::interval
 GROUP BY m.id, m.address
)
SELECT
 l.id AS listid,
 name,
 bounces,
 deliveries,
 bounces*100/deliveries AS percentage,
 bounces_last_7,
 deliveries_last_7,
 CASE WHEN deliveries_last_7>0 THEN bounces_last_7*100/deliveries_last_7 END AS percentage_last_7
FROM lists_list l
LEFT JOIN delivered ON l.id=delivered.listid
LEFT JOIN bounced ON bounced.list=delivered.listid
WHERE EXISTS (SELECT 1 FROM lists_listsubscription WHERE subscriber_id=%(subid)s AND list_id=l.id)
ORDER BY list""",
                {
                    'subid': self.sub.pk,
                }
            )
            context['all_listids'] = [l['listid'] for l in context['otherlists']]
        return context

    @transaction.atomic
    def post(self, request, listid, sub):
        if 'transientid' in request.POST:
            curs = connection.cursor()
            if 'complete delivery' in request.POST['submit']:
                curs.execute("UPDATE matched_bounces SET transient=%(transient)s WHERE deliveryid=(SELECT deliveryid FROM matched_bounces WHERE id=%(bounceid)s AND subscriber=%(subid)s) AND NOT transient=%(transient)s", {
                    'transient': 'transient' in request.POST['submit'],
                    'bounceid': int(request.POST['transientid']),
                    'subid': self.sub.id,
                })
                messages.info(request, "Marked {0} messages as {1}".format(
                    curs.rowcount,
                    'transient' in request.POST['submit'] and 'transient' or 'permanent',
                ))
            else:
                # Mark individual email
                curs.execute("UPDATE matched_bounces SET transient=NOT transient WHERE id=%(bounceid)s AND subscriber=%(subid)s", {
                    'bounceid': int(request.POST['transientid']),
                    'subid': self.sub.id,
                })
                messages.info(request, "Marked message as {0}".format(
                    'transient' in request.POST['submit'] and 'transient' or 'permanent',
                ))
            return HttpResponseRedirect(".")
        elif 'unsub' in request.POST:
            if request.POST.get('unsub') == '1':
                s = get_object_or_404(ListSubscription, list=self.list, subscriber=self.sub)

                log(Level.INFO, self.request.user, "Unsubscribed {0} from {1} from bounce".format(s.subscriber, s.list))
                messages.info(self.request, "Unsubscribed {0} from {1}".format(s.subscriber, s.list))
                log_unsubscription(s)
                notify_unsubscribed_user(s, request.POST.get('unsubnote'))
                s.delete()
            elif request.POST.get('unsub') == '2':
                # Unsubscribe from *all* specified lists
                if not request.user.is_superuser:
                    raise Http404("Nope")

                for s in ListSubscription.objects.filter(subscriber=self.sub, list__in=request.POST.get('listidlist').split(',')):
                    log(Level.INFO, self.request.user, "Unsubscribed {0} from {1} (part of all-lists unsubscription) from bounce".format(s.subscriber, s.list))
                    messages.info(self.request, "Unsubscribed {0} from {1}".format(s.subscriber, s.list))
                    log_unsubscription(s)
                    notify_unsubscribed_user(s, request.POST.get('unsubnote'))
                    s.delete()

            return HttpResponseRedirect("../../")
        else:
            return SimpleWebResponse(request, "Invalid form data")


class ManageModerators(LoginRequired, TemplateView):
    template_name = 'manage_moderators.html'

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_superuser:
            raise Http404("Nope")
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        d = super().get_context_data(**kwargs)
        d.update({
            'lists': List.objects.select_related('domain').prefetch_related('moderators', 'moderators__user').all().order_by('domain__name', 'name'),
            'moderators': Subscriber.objects.select_related('user').prefetch_related('moderator_of_lists', 'moderator_of_lists__domain').exclude(moderator_of_lists=None),
        })
        return d
