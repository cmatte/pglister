# Generated by Django 3.2.14 on 2023-09-27 09:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0057_neverblockregexp'),
    ]

    operations = [
        migrations.AddField(
            model_name='list',
            name='disable_list_headers',
            field=models.BooleanField(default=False, help_text='Disable setting list headers when forwarding email. This also disables DKIM list header enforcement.'),
        ),
        migrations.RunSQL("""
CREATE OR REPLACE VIEW mailinglists AS
   SELECT l.id AS id,
         l.name || '@' || d.name AS address,
      l.name AS name,
      d.name AS domain,
      l.moderation_level AS moderation_level,
      l.moderation_regex AS moderation_regex,
      l.spamscore_threshold AS spamscore_threshold,
      l.maxsizemoderate AS maxsizemoderate,
      l.maxsizedrop AS maxsizedrop,
      a.name AS archiveserver,
      replace(a.urlpattern, '%', l.name) AS archiveaddress,
      replace(d.lists_helppage, '%', l.name) AS helppage,
      a.maildomain AS archivedomain,
      l.blocklist_regex AS blocklist_regex,
      l.whitelist_regex AS whitelist_regex,
      l.tagged_delivery AS tagged_delivery,
      l.tagkey AS tagkey,
      l.taglistsource AS taglistsource,
      l.send_moderation_notices AS send_moderation_notices,
      l.cc_policy AS cc_policy,
      l.bcc_policy AS bcc_policy,
      l.ignore_global_mod_regex AS ignore_global_mod_regex,
      l.reject_to_cc AS reject_to_cc,
      a.mailurlpattern AS archivemailurlpattern,
      l.disable_list_headers AS disable_list_headers
   FROM lists_list l
   INNER JOIN lists_domain d ON d.id=l.domain_id
   LEFT JOIN lists_archiveserver a ON a.id=l.archivedat_id
"""),
    ]
