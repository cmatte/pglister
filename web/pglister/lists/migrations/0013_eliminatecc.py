# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0012_list_sorting'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscriber',
            name='eliminatecc',
            field=models.BooleanField(default=False, help_text="Don't receive an extra copy of mails when listed in To or CC fields"),
        ),
    ]
