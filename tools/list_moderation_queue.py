#!/usr/bin/python3 -u
#
# list_moderation_queue.py - list contents of moderatino queue, decoding fields
#                            for debugging purposes.
#

import os
import sys
from email import message_from_bytes

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../lib')))

from baselib.config import config


def recursive_print(msg):
    # We assume everything is directly printable because these are emails that we
    # created ourselves.
    b = msg.get_payload(decode=True)
    if b:
        print("---")
        print(b.decode('utf8', errors='ignore'))
        return
    b = msg.get_payload()
    if isinstance(b, str):
        print("---")
        print(b)
    elif isinstance(b, list):
        for p in b:
            recursive_print(p)
    else:
        print("UNKNOWN TYPE: {0}".format(type(b)))


if __name__ == "__main__":
    conn = config.conn('list_moderation_queue')
    curs = conn.cursor()

    curs.execute("SELECT id, sender, recipient, contents FROM moderation ORDER BY id")
    for id, sender, recipient, contents in curs.fetchall():
        print("--------------------------------------------------------------------------")
        print("Id:        {0}".format(id))
        print("Sender:    {0}".format(sender))
        print("Recipient: {0}".format(recipient))
        print("Contents:")
        msg = message_from_bytes(bytes(contents))
        for k, v in list(msg.items()):
            print("{0:30} {1}".format(k, v))
        recursive_print(msg)
        print("--------------------------------------------------------------------------")
