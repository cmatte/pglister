#!/usr/bin/python3 -u
#
# list_admin_out.py - clear admin out queue, in case messages are stuck there
#

import os
import sys
from email.parser import Parser

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../lib')))

from baselib.config import config

if __name__ == "__main__":
    conn = config.conn('list_admin_out')
    curs = conn.cursor()

    curs.execute("DELETE FROM admin_out")
    print("admin_out cleared.")
