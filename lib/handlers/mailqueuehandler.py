from .mailhandler import MailHandler
from baselib.misc import log


class MailQueueHandler(object):
    def __init__(self, conn):
        self.conn = conn

    def process_next(self):
        with self.conn.cursor() as curs:
            curs.execute("SELECT id, recipient, sender, messageid, contents FROM incoming_mail WHERE NOT failed ORDER BY id LIMIT 1 FOR UPDATE")
            ll = curs.fetchall()
            if len(ll) == 0:
                # Nothing left, so we're done
                self.conn.rollback()
                return False
            elif len(ll) != 1:
                raise Exception("Should not happen - LIMIT 1 returned more than 1")

            # Else we have an actual email to process
            try:
                id, recipient, sender, messageid, contents = ll[0]
                mail = MailHandler(self.conn, id, recipient, sender, messageid, contents)
                if mail.process():
                    # Success, so we delete the entry from the queue
                    curs.execute("DELETE FROM incoming_mail WHERE id=%(id)s", {'id': id})
                    log(curs, 0, 'mail', 'Done processing.', messageid)
                else:
                    # Failure! That's not good...
                    # Flag it as failed and leave it in the queue. That will
                    # also trigger any monitoring that looks at the length
                    # of the queue.
                    curs.execute("UPDATE incoming_mail SET failed='t' WHERE id=%(id)s", {'id': id})
            except Exception as ex:
                import traceback
                print("Exception: %s" % ex)
                print(traceback.format_exc())
                self.conn.rollback()
                curs.execute("UPDATE incoming_mail SET failed='t' WHERE id=%(id)s", {'id': id})
                self.conn.commit()
            else:
                # New transaction for the next mail
                self.conn.commit()
        return True
