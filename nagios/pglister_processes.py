#!/usr/bin/env python3
#
# pglister_processes.py - nagios monitor for pglister processes
#
# Note: monitors processes actually connected to the database, not the
#       process itself. If they're not connected to the db, they will not
#       do anything..
#

import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../lib')))

from baselib.config import config

if __name__ == "__main__":
    conn = config.conn('nagios_processes')
    curs = conn.cursor()

    issues = []
    total = 0

    curs.execute("WITH t AS (SELECT application_name, count(*) AS num FROM pg_stat_activity GROUP BY application_name) SELECT COALESCE(application_name, column1) AS app, COALESCE(num, 0) FROM t RIGHT JOIN (VALUES('cleanup'), ('eximcleaner'), ('mailsender'), ('moderationprocessor'), ('bounceprocessor'), ('mailprocessor')) vv ON t.application_name=vv.column1 ORDER BY application_name")
    for proc, num in curs.fetchall():
        if num == 0:
            issues.append("Process {0} is not connected".format(proc))
        total += num

    if issues:
        print("CRITICAL: {0}".format(', '.join(issues)))
        sys.exit(2)

    print("OK, {0} running connected".format(total))
    sys.exit(0)
