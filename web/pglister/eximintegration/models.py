from django.db import models


class RefreshDate(models.Model):
    viewname = models.CharField(max_length=64, null=False, blank=False, primary_key=True)
    lastrefresh = models.DateTimeField(null=False, blank=False)

    def __str__(self):
        return self.viewname
