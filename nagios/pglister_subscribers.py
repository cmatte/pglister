#!/usr/bin/env python3
#
# pglister_subscribers.py - nagios monitor for subscribers to restricted lists
#
# Uses the http rest api to call into pglister, so not expected to run locally
#
# The plugin takes an INI file and processes all entries in it.
# It should have the following format:
#
# [DEFAULT]
# rooturl=https://some.host.com/
#
# [list1]
# apikey=supersecret
# subscribers=a@foo.com,b@bar.com,c@baz.com
#

import sys
import os

import requests
import optparse
from configparser import ConfigParser


class List(object):
    def __init__(self, config, listname):
        self.config = config
        self.listname = listname
        self.subscribers = set([x.strip() for x in self.config.get(self.listname, 'subscribers').split(',')])

    def check(self):
        r = requests.get('{0}/api/list/{1}/'.format(
            self.config.get(self.listname, 'rooturl'),
            self.listname,
        ), headers={
            'X-Api-Key': self.config.get(self.listname, 'apikey'),
        })
        if r.status_code != 200:
            return (NagiosResult.CRITICAL, 'Invalid status code {0} for list {1}'.format(r.status_code, self.listname))
        j = r.json()
        current = set(j['subscribers'])
        if current.difference(self.subscribers):
            return (NagiosResult.CRITICAL, 'List {0} should not have member(s) {1}'.format(
                self.listname,
                ', '.join(current.difference(self.subscribers)),
            ))
        if self.subscribers.difference(current):
            return (NagiosResult.WARNING, 'List {0} is missing member(s) {1}'.format(
                self.listname,
                ', '.join(self.subscribers.difference(current)),
            ))
        return (NagiosResult.OK, None)


class NagiosResult(object):
    OK = 0
    WARNING = 1
    CRITICAL = 2
    levelnames = {
        OK: 'OK',
        WARNING: 'WARNING',
        CRITICAL: 'CRITICAL',
    }


class NagiosResultCollector(object):
    def __init__(self):
        self.worst_status = NagiosResult.OK
        self.messages = []

    def append(self, status, message):
        if status > self.worst_status:
            self.worst_status = status
        if message:
            self.messages.append(message)

    def exit(self):
        print("%s: %s" % (
            NagiosResult.levelnames[self.worst_status],
            ' :: '.join(self.messages),
        ))
        sys.exit(self.worst_status)


if __name__ == "__main__":
    parser = optparse.OptionParser()
    parser.add_option('-f', dest='configfile', help='Name of configuration file')

    (opts, args) = parser.parse_args()

    if not opts.configfile:
        parser.error("Configuration file must be specified")

    if not os.path.isfile(opts.configfile):
        print("File %s not found" % opts.configfile)
        sys.exit(3)

    c = ConfigParser()
    c.read(opts.configfile)

    lists = [List(c, listname) for listname in c.sections()]

    result = NagiosResultCollector()
    for l in lists:
        result.append(*l.check())

    result.exit()
