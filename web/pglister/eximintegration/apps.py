from django.apps import AppConfig
from django.db.models.signals import pre_migrate
from django.db import connection
from django.conf import settings

import os
import sys


class CommandBuilder(object):
    def __init__(self):
        self.commands = []
        self.curs = connection.cursor()

    def add(self, vquery, *args):
        if self.commands:
            doit = True
        else:
            self.curs.execute(vquery)
            if self.curs.rowcount == 0:
                doit = True
            else:
                doit = False
        if doit:
            self.commands.extend(args)

    def print(self):
        # Get username for replacement
        self.curs.execute("SELECT CURRENT_USER")
        u = self.curs.fetchone()[0]

        for c in self.commands:
            print("{};".format(c.replace('%CURRENTUSER%', u)))


# Special handling of dependencies that have to be set up as superuser
def handle_pre_migrate(sender, **kwargs):
    commands = CommandBuilder()
    commands.add("SELECT 1 FROM pg_extension WHERE extname='file_fdw'",
                 "CREATE EXTENSION IF NOT EXISTS file_fdw")
    commands.add("SELECT 1 FROM pg_foreign_server WHERE srvname='file_fdw'",
                 "CREATE SERVER IF NOT EXISTS file_fdw FOREIGN DATA WRAPPER file_fdw")
    commands.add("SELECT 1 FROM pg_namespace WHERE nspname='eximintegration'",
                 "CREATE SCHEMA IF NOT EXISTS eximintegration",
                 "GRANT CREATE, USAGE ON SCHEMA eximintegration TO %CURRENTUSER%")
    commands.add(
        "SELECT 1 FROM pg_class WHERE relnamespace='eximintegration'::regnamespace AND relname='_raw_eximqueue'",
        "CREATE FOREIGN TABLE eximintegration._raw_eximqueue(jdata jsonb NOT NULL) SERVER file_fdw OPTIONS (PROGRAM '{}')".format(
            os.path.join(os.path.abspath(os.path.join(settings.BASE_DIR, '../bin/exiqjson.py'))),
        ),
        "GRANT SELECT ON eximintegration._raw_eximqueue TO %CURRENTUSER%",
    )

    if commands.commands:
        print("You need to run the following commands AS SUPERUSER in the database before you can migrate eximintegration")
        commands.print()
        sys.exit(1)


class EximAppConfig(AppConfig):
    name = 'pglister.eximintegration'

    def ready(self):
        pre_migrate.connect(handle_pre_migrate, sender=self)
