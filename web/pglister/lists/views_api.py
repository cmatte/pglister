from django.shortcuts import get_object_or_404, Http404
from django.http import HttpResponse
from django.db import connection, transaction
from django.db.models import Q
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django import forms
from django.core.validators import validate_email

from lib.baselib.misc import generate_random_token
from .models import List, ArchiveServer

import json
import datetime


def _json_format(o):
    if isinstance(o, datetime.datetime):
        return o.isoformat()
    return o


class ListApi(View):
    @transaction.atomic
    def get(self, request, listname):
        try:
            (listpart, domainpart) = listname.split('@', 1)
        except ValueError:
            raise Http404("Invalid list name format")
        l = List.objects.filter(name=listpart, domain__name=domainpart, apikey_ro=request.META.get('HTTP_X_API_KEY', None)).exclude(apikey_ro__isnull=True).exclude(apikey_ro='')

        if len(l) != 1:
            raise Http404("Not found or invalid API key")
        l = l[0]

        return HttpResponse(json.dumps({
            'name': str(l),
            'subscribers': [s.email for s in l.subscribers.all().order_by('email')],
        }, indent=1), content_type='application/json')


class SubscribersApi(View):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        try:
            (self.listpart, self.domainpart) = kwargs['listname'].split('@', 1)
        except ValueError:
            raise Http404("Invalid list name format")

        apikey = request.META.get('HTTP_X_API_KEY')
        if apikey is None or apikey == '':
            raise Http404("API key missing")

        ll = List.objects.filter(name=self.listpart, domain__name=self.domainpart).filter(
            Q(apikey_ro=apikey) | Q(apikey_rw=apikey)
        )
        if not ll:
            raise Http404("List not found or API key invalid")

        self.list = ll[0]
        self.readwrite = self.list.apikey_rw == apikey

        return super(SubscribersApi, self).dispatch(request, *args, **kwargs)

    @transaction.atomic
    def get(self, request, listname):
        return HttpResponse(
            json.dumps(
                [{'email': s.email} for s in self.list.subscribers.all().order_by('email')],
                indent=1
            ), content_type='application/json')

    @transaction.atomic
    def put(self, request, listname):
        if not self.readwrite:
            return HttpResponse("Read/write access not allowed", status=500)

        try:
            j = json.loads(request.body.decode('utf8'))
        except ValueError:
            return HttpResponse("Unparseable input format", status=500)

        if not isinstance(j, list):
            return HttpResponse("Invalid input format, must be list", status=500)

        emails = []
        for e in j:
            if not isinstance(e, dict):
                return HttpResponse("Invalid input format, must be dict (%s)" % e, status=500)
            e = e['email'].lower()
            try:
                validate_email(e)
            except forms.ValidationError:
                return HttpResponse("Invalid email: %s" % j, status=500)
            emails.append(e)

        # Start by creating any subscriberaddresses that don't exist yet.
        # We could perhaps optimize this by using server side token
        # generation, but let's do it the easy way for now.
        curs = connection.cursor()
        curs.execute("SELECT e FROM UNNEST(%(l)s) e(e) WHERE NOT EXISTS (SELECT 1 FROM lists_subscriberaddress WHERE email=e)", {
            'l': emails,
        })
        for email, in curs.fetchall():
            curs.execute("INSERT INTO lists_subscriberaddress (email, confirmed, blocked, token) VALUES (%(email)s, 't', 'f', %(token)s) ON CONFLICT DO NOTHING", {
                'email': email,
                'token': generate_random_token(),
            })

        curs.execute("DELETE FROM lists_listsubscription ls USING lists_subscriberaddress sa WHERE ls.subscriber_id=sa.id AND ls.list_id=%(listid)s AND NOT email=ANY(%(l)s) RETURNING email", {
            'listid': self.list.id,
            'l': emails,
        })
        deleted = [e for e, in curs.fetchall()]

        # Silly django does not generate ON CASCADE DELETE; so we have to do it ourselves
        curs.execute("DELETE FROM lists_listsubscription_tags WHERE NOT EXISTS (SELECT 1 FROM lists_listsubscription ls WHERE ls.id=listsubscription_id)")

        curs.execute("WITH t AS (INSERT INTO lists_listsubscription (nomail, list_id, subscriber_id, subscription_confirmed) SELECT 'f', %(listid)s, sa.id, 't' FROM lists_subscriberaddress sa WHERE sa.email=ANY(%(l)s) AND NOT EXISTS (SELECT 1 FROM lists_listsubscription ls2 WHERE ls2.subscriber_id=sa.id AND ls2.list_id=%(listid)s) ON CONFLICT DO NOTHING RETURNING subscriber_id) SELECT email FROM lists_subscriberaddress sa2 INNER JOIN t ON t.subscriber_id=sa2.id", {
            'listid': self.list.id,
            'l': emails,
        })
        added = [e for e, in curs.fetchall()]

        return HttpResponse(json.dumps({
            'added': added,
            'deleted': deleted,
        }, indent=1), content_type='application/json')


class ArchivesApi(View):
    @transaction.atomic
    def get(self, request, servername, op):
        self.archiveserver = get_object_or_404(ArchiveServer, name=servername, apikey=request.META.get('HTTP_X_API_KEY', None), apikey__isnull=False)
        if op == 'lists':
            return self.get_lists(request.GET.get('subscribers', '0') == '1')
        else:
            raise Http404("Unknown op")

    def get_lists(self, with_subscribers):
        curs = connection.cursor()
        if with_subscribers:
            curs.execute("SELECT l.id AS listid, l.name AS listname, d.name AS domain, l.shortdesc, l.longdesc, json_build_object('id', g.id, 'groupname', g.groupname, 'subscriber_access', g.subscriber_access) AS group, COALESCE(array_agg(u.username) FILTER (WHERE u.username IS NOT NULL), ARRAY[]::text[]) AS subscribers FROM lists_list l INNER JOIN lists_domain d ON l.domain_id=d.id INNER JOIN lists_listgroup g ON l.group_id=g.id LEFT JOIN mailinglist_subscribers s ON s.listid=l.id LEFT JOIN auth_user u ON u.id=s.userid WHERE l.archivedat_id=%(archiveid)s GROUP BY l.id, d.id, g.id ORDER BY 2,1", {
                'archiveid': self.archiveserver.id,
            })
        else:
            curs.execute("SELECT l.id AS listid, l.name AS listname, d.name AS domain, l.shortdesc, l.longdesc, json_build_object('id', g.id, 'groupname', g.groupname, 'subscriber_access', g.subscriber_access) AS group FROM lists_list l INNER JOIN lists_domain d ON l.domain_id=d.id INNER JOIN lists_listgroup g ON l.group_id=g.id WHERE l.archivedat_id=%(archiveid)s GROUP BY l.id, d.id, g.id ORDER BY 2,1", {
                'archiveid': self.archiveserver.id,
            })
        columns = [col[0] for col in curs.description]
        return HttpResponse(
            json.dumps(
                [dict(zip(columns, row)) for row in curs.fetchall()],
                indent=1
            ), content_type='application/json')
