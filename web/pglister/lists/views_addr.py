from django.shortcuts import get_object_or_404, Http404
from django.http import HttpResponseRedirect
from django.views.generic import View, FormView
from django.contrib import messages
from django.db import transaction
from django.conf import settings

from datetime import datetime, timedelta

from .models import Subscriber, SubscriberAddress, SubscriberAddressToken
from .forms import ConfirmForm, AddMailForm
from .util import LoginRequired, send_template_mail

from pglister.listlog.util import log, Level

from lib.baselib.misc import generate_random_token


def _send_confirmation_email(sa, user):
    # Generate the confirmation email
    send_template_mail(
        'mail/confirm_email.txt',
        settings.CONFIRM_SENDER_ADDRESS,
        settings.CONFIRM_SENDER_NAME,
        sa.email,
        sa.subscriber and sa.subscriber.fullname or '',
        "Confirmation of email address",
        {
            'webroot': settings.WEB_ROOT,
            'sysname': settings.SYSTEM_NAME,
            'token': sa.subscriberaddresstoken.token,
            'user': user,
        }
    )


class AddMail(LoginRequired, FormView):
    template_name = 'baseform.html'
    success_url = '/manage/'
    form_class = AddMailForm
    savebutton = 'Add mail'
    cancelurl = '/manage/'

    def dispatch(self, *args, **kwargs):
        if settings.USE_PG_COMMUNITY_AUTH:
            raise Http404()
        return super().dispatch(*args, **kwargs)

    @transaction.atomic
    def form_valid(self, form):
        subscriber = get_object_or_404(Subscriber, user=self.request.user)

        # Rate-limit number of active tokens
        if subscriber.subscriberaddress_set.filter(subscriberaddresstoken__isnull=False).count() > 4:
            messages.error(self.request, "You have too many pending address confirmations. Confirm or delete one or more of them before adding another.")
            return super(AddMail, self).form_valid(form)

        # Check if a migrated address exists, in which case we want to latch on to that
        # one. The form will have validated that this is the only case where an address
        # can exist when we get this far.
        try:
            sa = SubscriberAddress.objects.get(email=form.cleaned_data['email'])
            # Can-never-happen checks
            if not sa.confirmed:
                raise Exception("Address is not confirmed, as it should be for migrated addresses!")
            if sa.subscriber:
                raise Exception("Subscriber already set!")
            # Else we have a migrated subscriberaddress. We won't assign it to the subscriber
            # user until it has been confirmed of course, but we have to create the token
            # at this point.
        except SubscriberAddress.DoesNotExist:
            # Create an unconfirmed SubscriberAddress
            sa = SubscriberAddress(subscriber=subscriber, email=form.cleaned_data['email'].lower(), confirmed=False, token=generate_random_token())
            sa.save()

        # Create a token for this to be confirmed
        token = SubscriberAddressToken(subscriberaddress=sa,
                                       token=generate_random_token())
        token.save()

        _send_confirmation_email(sa, self.request.user)
        log(Level.INFO, self.request.user, "Sent confirmation token to {0}".format(sa.email))

        messages.info(self.request, "An email has been sent to {0} with instructions for how to confirm this address.".format(sa.email))

        return super(AddMail, self).form_valid(form)


class ConfirmMail(LoginRequired, View):

    @transaction.atomic
    def get(self, request, token):
        if settings.USE_PG_COMMUNITY_AUTH:
            raise Http404()

        subscriber = get_object_or_404(Subscriber, user=self.request.user)
        token = get_object_or_404(SubscriberAddressToken, token=token)
        sa = token.subscriberaddress

        # Does this token match up with the current user?
        if sa.subscriber == subscriber:
            # Yes it does! That means this email is now confirmed.
            sa.confirmed = True
            sa.save()

            # We don't need the token anymore...
            token.delete()

            log(Level.INFO, self.request.user, "Confirmed email {0} using token.".format(sa.email))
            messages.info(request, "Email address %s has been confirmed." % sa.email)
        elif not sa.subscriber:
            # No subscriber means it's a migrated connection
            if not sa.confirmed:
                raise Exception("Migrated connections should always have confirmed set!")
            # Proper migrated connection, and email is confirmed, so connect this
            # subscriberaddress to the current subscriber. It's already confirmed,
            # so we don't need to touch that one.
            sa.subscriber = subscriber
            sa.save()

            # We don't need the token anymore...
            token.delete()

            log(Level.INFO, self.request.user, "Claimed migrated email {0} using token.".format(sa.email))
            messages.info(request, "Email address %s has been connected with your account." % sa.email)
        else:
            # Token belongs to a different user
            log(Level.ERROR, self.request.user, "Attempted to claim email {0} using token, but process started from {1}.".format(sa.email, sa.subscriber.user.username))
            messages.error(request, "Email address %s is being claimed by a different user." % sa.email)

        return HttpResponseRedirect("/manage/")


class DelMail(LoginRequired, FormView):
    template_name = 'baseform.html'
    success_url = '/manage/'
    form_class = ConfirmForm
    savebutton = 'Delete mail address'
    cancelurl = '/manage/'

    def dispatch(self, *args, **kwargs):
        if settings.USE_PG_COMMUNITY_AUTH:
            raise Http404()
        return super().dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kw = super(DelMail, self).get_form_kwargs()

        subscriber = get_object_or_404(Subscriber, user=self.request.user)
        subaddr = get_object_or_404(SubscriberAddress, pk=self.kwargs['subid'], subscriber=subscriber)

        kw['prompt'] = "Please confirm that you want to delete the email {0}.".format(subaddr.email)
        return kw

    @transaction.atomic
    def form_valid(self, form):
        subscriber = get_object_or_404(Subscriber, user=self.request.user)
        subaddr = get_object_or_404(SubscriberAddress, pk=self.kwargs['subid'], subscriber=subscriber)

        # Ensure that this subscriberaddress isn't subscribed to anything
        if subaddr.has_subscriptions:
            # This should not happen since the link isn't available, but
            # catch it just in case.
            messages.error(self.request, "This email address has active subscriptions")
        else:
            em = subaddr.email
            subaddr.delete()
            log(Level.INFO, self.request.user, "Removed email address {0}.".format(em))
            messages.info(self.request, "Your email address {0} has been removed.".format(em))

        return super(DelMail, self).form_valid(form)


class Resend(LoginRequired, View):
    @transaction.atomic
    def get(self, request, subid):
        if settings.USE_PG_COMMUNITY_AUTH:
            raise Http404()

        subscriber = get_object_or_404(Subscriber, user=self.request.user)
        subaddr = get_object_or_404(SubscriberAddress, pk=subid, subscriber=subscriber)

        if not hasattr(subaddr, 'subscriberaddresstoken'):
            # Should never happen
            messages.error(request, "Email address {0} has no token.".format(subaddr.email))
        else:
            # Verify that we didn't just send this thing, to avoid spamming somebody
            # through it.
            if datetime.now() - subaddr.subscriberaddresstoken.tokensent < timedelta(minutes=5):
                messages.error(request, "Cannot resend verification email in less than 5 minutes.")
                return HttpResponseRedirect("/manage/")

            # Ok, resend. While at it also update the generation date
            # for the token, to make sure it doesn't expire.
            subaddr.subscriberaddresstoken.tokensent = datetime.now()
            subaddr.subscriberaddresstoken.save()

            _send_confirmation_email(subaddr, self.request.user)

            log(Level.INFO, self.request.user, "Re-sent confirmation token to {0}".format(subaddr.email))
            messages.info(request, "A new verification email has been sent to {0}.".format(subaddr.email))

        return HttpResponseRedirect("/manage/")


class Block(LoginRequired, View):
    @transaction.atomic
    def get(self, request, subid):
        subscriber = get_object_or_404(Subscriber, user=self.request.user)
        subaddr = get_object_or_404(SubscriberAddress, pk=subid, subscriber=subscriber)

        subaddr.blocked = not subaddr.blocked
        subaddr.save()
        if subaddr.blocked:
            messages.info(request, "Address {0} blocked from posting".format(subaddr.email))
        else:
            messages.info(request, "Address {0} no longer blocked from posting".format(subaddr.email))

        return HttpResponseRedirect("/manage/")
