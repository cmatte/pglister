# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import pglister.lists.models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0016_personal_blacklist'),
    ]

    operations = [
        migrations.AddField(
            model_name='list',
            name='blacklist_regex',
            field=models.TextField(help_text='Regular expressions to match against sender addresses and moderate posts from matching. One regex per line.', blank=True, validators=[pglister.lists.models.ValidateRegexpList]),
        ),
        migrations.AddField(
            model_name='list',
            name='whitelist_regex',
            field=models.TextField(help_text='Regular expressions to match against sender addresses and allow posts from matching. One regex per line.', blank=True, validators=[pglister.lists.models.ValidateRegexpList]),
        ),
    ]
