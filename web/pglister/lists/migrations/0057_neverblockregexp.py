# Generated by Django 3.2.11 on 2022-05-16 13:05

from django.db import migrations, models
import pglister.lists.models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0056_workaround_django32_2'),
    ]

    operations = [
        migrations.CreateModel(
            name='NeverBlockRegexp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('regexp', models.CharField(help_text='Regular expression to always disallow blocklisting of', max_length=255, validators=[pglister.lists.models.ValidateRegexp])),
            ],
        ),
    ]
