import re
import urllib.parse
import requests
import psycopg2.extras

from .config import config
from .misc import log


class ModerationLevels(object):
    NONE = 0
    SUBSCRIBERS = 1
    GLOBAL = 2
    ALL = 3
    DOUBLE = 4

    choices = (
        (NONE, 'No moderation'),
        (SUBSCRIBERS, 'Direct subscribers can post'),
        (GLOBAL, 'Global confirmed addresses can post'),
        (ALL, 'All emails are moderated'),
        (DOUBLE, 'All emails are double-moderated'),
    )


class SubscriptionPolicies(object):
    OPEN = 0
    CONFIRMED = 1
    MANAGED = 2

    choices = (
        (OPEN, 'Anybody can subscribe'),
        (CONFIRMED, 'Requires moderator approval'),
        (MANAGED, 'Moderator managed, unlisted'),
    )


class CcPolicies(object):
    ALLOW = 0
    MODERATE = 1
    DISCARD = 2

    choices = (
        (ALLOW, 'Allow posts when multiple lists are in to/cc'),
        (DISCARD, 'Discard posts if multiple lists are in to/cc'),
        (MODERATE, 'Moderate posts if multiple lists are in to/cc')
    )


class BccPolicies(object):
    ALLOW = 0
    MODERATE = 1
    DISCARD = 2

    choices = (
        (ALLOW, 'Allow posts even when list is not in to/cc'),
        (MODERATE, 'Moderate posts when list is not in to/cc'),
        (DISCARD, 'Discard posts when list is not in to/cc'),
    )


class ModerationOption():
    APPROVE = 1
    WHITELIST = 2
    DISCARD = 3
    REJECT = 4
    UNSUBSCRIBE = 5
    BLOCKLIST_SENDER = 6
    REJECT_UNSUB = 7
    BLOCKLIST_DOMAIN = 8


class ModerationReason(object):
    OTHER = 0
    SENDER_NOTSUBSCRIBER_LIST = 1
    SENDER_NOT_CONFIRMED = 2
    ALL_POSTS_HELD = 3
    SENDER_BLOCKLISTED = 4
    SIZE = 5
    SPAM = 6
    UNSUBSCRIBE = 7
    MODERATELIST = 8
    MODERATEREGEX = 9
    TAGERROR = 10
    BCC = 11
    GLOBALMODERATEREGEX = 12
    CC = 13
    DKIMINVALID = 14
    DKIMLISTHEADER = 15

    _messages = {
        OTHER: 'Other reason: {0}',
        SENDER_NOTSUBSCRIBER_LIST: 'sender is not a subscriber of the list',
        SENDER_NOT_CONFIRMED: 'sender is not a confirmed email address',
        ALL_POSTS_HELD: 'all posts held for moderation',
        SENDER_BLOCKLISTED: 'sender address matches blocklist pattern',
        SIZE: '{0}',
        SPAM: '{0}',
        UNSUBSCRIBE: 'message looks like a possible unsubscription request ({0})',
        MODERATELIST: 'referenced messageid {0} found on moderation list',
        MODERATEREGEX: 'message matches moderation regex {0}',
        TAGERROR: 'tag processing error: {0}',
        BCC: 'list name or aliases was not in to or cc field',
        GLOBALMODERATEREGEX: 'message matches global moderation regex for {0}',
        CC: 'message was CCed to multiple lists',
        DKIMINVALID: 'message has an invalid DKIM header',
        DKIMLISTHEADER: 'message is DKIM signing list headers',
    }

    def __init__(self, reason, extra=''):
        self.reason = reason
        self.extra = extra

    def full_string(self):
        return self._messages[self.reason].format(self.extra)

    def get_moderation_options(self, is_subscribed, is_superuser):
        yield (ModerationOption.APPROVE, "Approve")

        if self.reason in (self.SENDER_NOTSUBSCRIBER_LIST, self.SENDER_NOT_CONFIRMED):
            yield (ModerationOption.WHITELIST, "Whitelist for this list")

        yield (ModerationOption.DISCARD, "Discard")
        yield (ModerationOption.REJECT, "Reject")

        if is_subscribed and self.reason == self.UNSUBSCRIBE:
            yield (ModerationOption.UNSUBSCRIBE, "Unsubscribe")

        if is_superuser and self.reason in (self.SENDER_NOTSUBSCRIBER_LIST, self.SENDER_NOT_CONFIRMED,
                                            self.SPAM, self.BCC, self.CC, self.SIZE, self.UNSUBSCRIBE,
                                            self.DKIMINVALID, self.DKIMLISTHEADER,
                                            ):
            yield (ModerationOption.BLOCKLIST_SENDER, "Blocklist sender")
            yield (ModerationOption.BLOCKLIST_DOMAIN, "Blocklist sender domain")

        yield (ModerationOption.REJECT_UNSUB, "Reject unsub")


class MailingList(object):
    def __init__(self, conn, id, address, name, domain, moderation_level, moderation_regex, spamscore_threshold, maxsize, maxsizedrop, archive_server, archive_address, helppage, archive_domain, archive_mailurlpattern, blocklist_regex, whitelist_regex, tagged_delivery, tagkey, taglistsource, send_moderation_notices, cc_policy, bcc_policy, ignore_global_mod, reject_to_cc, disable_list_headers):
        """
        Internal, do not call directly! Instead use constructor methods:
        MailingList.get_by_address(address)
        """

        self.conn = conn
        self.id = id
        self.address = address
        self.name = name
        self.domain = domain
        self.moderation_level = moderation_level
        self.moderation_regexes = [re.compile(r, re.MULTILINE) for r in moderation_regex.splitlines()]
        self.spamscore_threshold = spamscore_threshold
        self.maxsize = maxsize
        self.maxsizedrop = maxsizedrop
        self.archive_server = archive_server
        self.archive_address = archive_address
        self.archive_mailurlpattern = archive_mailurlpattern
        self.helppage = helppage
        self.archive_domain = archive_domain
        self.blocklist_regexes = [re.compile(r, re.MULTILINE) for r in blocklist_regex.splitlines()]
        self.whitelist_regexes = [re.compile(r, re.MULTILINE) for r in whitelist_regex.splitlines()]
        self.tagged_delivery = tagged_delivery
        self.tagkey = tagkey
        self.taglistsource = taglistsource
        self.send_moderation_notices = send_moderation_notices
        self.cc_policy = cc_policy
        self.bcc_policy = bcc_policy
        self.ignore_global_mod = ignore_global_mod
        self.reject_to_cc = reject_to_cc
        self.disable_list_headers = disable_list_headers

        # Default spamscore
        if not self.spamscore_threshold:
            self.spamscore_threshold = int(config.get('defaults', 'spamscore_threshold'))

        # Default size limits
        if not maxsize:
            self.maxsize = int(config.get('defaults', 'size_moderate'))
        if not maxsizedrop:
            self.maxsizedrop = int(config.get('defaults', 'size_drop'))

    _SELECTFIELDS = "id, address, name, domain, moderation_level, moderation_regex, spamscore_threshold, maxsizemoderate, maxsizedrop, archiveserver, archiveaddress, helppage, archivedomain, archivemailurlpattern, blocklist_regex, whitelist_regex, tagged_delivery, tagkey, taglistsource, send_moderation_notices, cc_policy, bcc_policy, ignore_global_mod_regex, reject_to_cc, disable_list_headers"

    @staticmethod
    def get_by_address(conn, address):
        """
        Return a MailingList object representing a list with the specified
        email address.
        """

        with conn.cursor() as curs:
            curs.execute("SELECT {0} FROM mailinglists WHERE address=%(address)s".format(MailingList._SELECTFIELDS), {
                'address': address,
            })
            rr = curs.fetchall()
            if len(rr) == 1:
                return MailingList(conn, *rr[0])
            else:
                return None

    @staticmethod
    def get_by_id(conn, id):
        """
        Return a MailingList object representing a list with the specified id.
        """

        with conn.cursor() as curs:
            curs.execute("SELECT {0} FROM mailinglists WHERE id=%(id)s".format(MailingList._SELECTFIELDS), {
                'id': id,
            })
            rr = curs.fetchall()
            if len(rr) == 1:
                return MailingList(conn, *rr[0])
            else:
                return None

    def moderation_reason(self, sender, messageid):
        """
        Check if a sender requires moderation to post to this list, and if
        so, return the reason why moderation is needed. If moderation
        is not needed, return None.

        """
        curs = self.conn.cursor()

        # If there are whitelists or blocklists, scan those first
        if self.whitelist_regexes:
            for r in self.whitelist_regexes:
                if r.search(sender):
                    # Found on whitelist, so no moderation on this
                    # address.
                    log(curs, 0, 'mail',
                        'Mail from {0} matched whitelist pattern, posting without moderation'.format(sender),
                        messageid)
                    return None

        if self.blocklist_regexes:
            for r in self.blocklist_regexes:
                if r.search(sender):
                    # Found in blocklist
                    return ModerationReason(ModerationReason.SENDER_BLOCKLISTED)

        # No match on white or blocklists, so move on to regular policies
        if self.moderation_level == ModerationLevels.NONE:
            return None
        if self.moderation_level == ModerationLevels.SUBSCRIBERS:
            # Check if sender is a subscriber to this list
            curs.execute("SELECT EXISTS (SELECT 1 FROM mailinglist_subscribers WHERE listid=%(id)s AND email=%(email)s) OR EXISTS (SELECT 1 FROM mailinglist_whitelist WHERE listid=%(id)s AND email=%(email)s) OR EXISTS (SELECT 1 FROM mailinglist_globalwhitelist WHERE email=%(email)s)", {
                'id': self.id,
                'email': sender,
            })
            if not curs.fetchone()[0]:
                return ModerationReason(ModerationReason.SENDER_NOTSUBSCRIBER_LIST)
            return None
        if self.moderation_level == ModerationLevels.GLOBAL:
            # Check if the sender has a registered and verified email address
            # on this system (regardless of if it's subscribed to anything)
            # Also check the per-list whitelist!
            curs.execute("""SELECT
EXISTS (
  SELECT 1 FROM lists_subscriberaddress sa WHERE email=%(email)s AND sa.confirmed AND NOT sa.blocked
)
OR EXISTS (
  SELECT 1 FROM mailinglist_globalwhitelist WHERE email=%(email)s
)
OR EXISTS (SELECT 1 FROM mailinglist_whitelist WHERE listid=%(id)s AND email=%(email)s)""", {
                'id': self.id,
                'email': sender,
            })
            if not curs.fetchone()[0]:
                return ModerationReason(ModerationReason.SENDER_NOT_CONFIRMED)
            return None
        if self.moderation_level == ModerationLevels.ALL:
            return ModerationReason(ModerationReason.ALL_POSTS_HELD)
        if self.moderation_level == ModerationLevels.DOUBLE:
            return ModerationReason(ModerationReason.ALL_POSTS_HELD)
        return ModerationReason(ModerationReason.OTHER, "Unknown")

    def get_moderators(self):
        """
        Return a list of (email, name) combinations for all moderators of this list.
        """

        curs = self.conn.cursor()
        curs.execute("SELECT email, name FROM mailinglist_moderators WHERE listid=%(id)s", {
            'id': self.id,
        })
        return curs.fetchall()

    def writeheaders(self, buf, messageid):
        """
        Write the appropriate RFC2369 and RFC2919 headers to the
        buffer at the current location.
        """
        if self.disable_list_headers:
            return

        buf.write("List-Id: <{0}>\r\n".format(self.address.replace('@', '.')).encode('utf8'))
        buf.write("List-Help: <{0}>\r\n".format(self.helppage).encode('utf8'))
        buf.write("List-Subscribe: <{0}>\r\n".format(self.helppage).encode('utf8'))
        buf.write("List-Post: <mailto:{0}>\r\n".format(self.address).encode('utf8'))
        buf.write("List-Owner: <mailto:{0}>\r\n".format(self.owner_address()).encode('utf8'))
        if self.archive_address:
            buf.write("List-Archive: <{0}>\r\n".format(self.archive_address).encode('utf8'))
        if self.archive_mailurlpattern:
            msgid = urllib.parse.quote(messageid.lstrip("<").rstrip(">"))
            buf.write("Archived-At: <{0}>\r\n".format(self.archive_mailurlpattern.replace("%", msgid)).encode('utf-8'))
        buf.write("Precedence: bulk\r\n".encode('utf8'))

    def owner_address(self):
        """
        Return the owner address for this list. Will automatically be on
        the format where:
        llll@domain.com -> llll-owner@domain.com
        """
        return self.address.replace('@', '-owner@')

    def owner_name(self):
        """
        Return a name for the owner of this list. Will automatically be on
        the format where:
        lll@domain.com -> "lll owner"
        """
        return "{0} Owner".format(self.name)

    def archive_submit_address(self):
        """
        Return the email address to submit to the archives for this list.
        """
        if self.archive_domain:
            # We only support a flat namespace per server, so just put
            # the list name and the server name.
            return "{0}@{1}".format(self.name, self.archive_domain)
        else:
            return None

    def moderator_notice_address(self):
        """
        Return address used to send moderation notices. For now, we just
        return the owner address.
        """
        return self.owner_address()

    def moderator_notice_name(self):
        """
        Return name used to send moderation notices. For now, we just
        return the owner name.
        """
        return self.owner_name()

    def unsubscribe_address(self):
        """
        Return address that will initiate an unsubscribe request to this
        list. This request will still need to be double-verified, but
        it's good manners to allow unsubscribe via email.

        This address needs to be list-global because we don't change
        the *contents* of the email between the recipients, only the
        VERP. Thus, use the llll-unsubscribe@domain.com format, which
        will generate an automatic confirmation email.
        """
        return self.address.replace('@', '-unsubscribe@')

    def refresh_tags(self):
        # Refresh the tags for this specific list.
        # API call is expected to use https, so we trust that, no need for a separate
        # validation.
        with self.conn.cursor() as curs:
            try:
                r = requests.get(self.taglistsource)
                if r.status_code != 200:
                    log(curs, 2, 'mail', "Failed to load taglist for {0}: status code {1}".format(self.name, r.status_code))
                    return
                taglist = r.json()
                curs.execute("""WITH t AS (SELECT * FROM json_to_recordset(%(json)s) AS (urlname text, name text, description text, sortkey int)),
ins AS (INSERT INTO lists_listtag (list_id, tag, name, description, sortkey) SELECT %(listid)s, urlname, name, description, sortkey FROM t WHERE NOT EXISTS (SELECT 1 FROM lists_listtag lt2 WHERE lt2.list_id=%(listid)s AND lt2.tag=t.urlname) RETURNING tag),
upd AS (UPDATE lists_listtag SET name=t.name, description=t.description, sortkey=t.sortkey FROM t WHERE lists_listtag.list_id=%(listid)s AND lists_listtag.tag=t.urlname AND NOT (lists_listtag.name=t.name AND lists_listtag.description=t.description AND lists_listtag.sortkey=t.sortkey) RETURNING tag),
del AS (DELETE FROM lists_listtag WHERE list_id=%(listid)s AND NOT EXISTS (SELECT 1 FROM t WHERE t.urlname=lists_listtag.tag) RETURNING tag)
SELECT 'added ' || tag FROM ins
UNION ALL
SELECT 'updated ' || tag FROM upd
UNION ALL
SELECT 'deleted ' || tag FROM del
""", {
                    'listid': self.id,
                    'json': psycopg2.extras.Json(taglist['tags']),
                })
                if curs.rowcount:
                    log(curs, 0, 'mail', "Refreshed tags for {}: {}".format(
                        self.name,
                        ', '.join([t for t, in curs.fetchall()]),
                    ))

                # Unfortunately, django's ORM is really stupid and doesn't create ON DELETE CASCADE foreign keys even when they
                # are defined as such in the ORM, it does it all in the application. So explicitly go remove those subscriptions
                # that are tied to any now removed tags (because we really don't have much choice)
                curs.execute("DELETE FROM lists_listsubscription_tags WHERE NOT EXISTS (SELECT 1 FROM lists_listtag WHERE listtag_id=lists_listtag.id) RETURNING id")
                if curs.rowcount:
                    log(curs, 0, 'mail', "Removed {} tag subscriptions from {} because of tag updates".format(curs.rowcount, self.name))
            except Exception as e:
                log(curs, 2, 'mail', "Failed to load taglist for {0}: Exception {1}".format(self.name, e))
                return
