from django.contrib import admin
from django import forms

from .models import Domain, List, ListGroup, ListAlias, ArchiveServer
from .models import ListWhitelist, GlobalWhitelist, GlobalBlocklist, NeverBlockRegexp
from .models import MessageidModerationList
from .models import GlobalModerationList
from .models import Subscriber
from .models import CannedRejectResponse, CannedUnsubscriptionNotice


class ListWhitelistAdminForm(forms.ModelForm):
    class Meta:
        model = ListWhitelist
        exclude = ()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['list'].widget.can_add_related = False
        self.fields['list'].widget.can_change_related = False


class ListWhitelistAdmin(admin.ModelAdmin):
    form = ListWhitelistAdminForm
    list_display = ('list', 'address', 'added_at')


class ListAdminForm(forms.ModelForm):
    class Meta:
        model = List
        exclude = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for f in ['domain', 'group', 'archivedat', 'moderators']:
            self.fields[f].widget.can_add_related = False
            self.fields[f].widget.can_change_related = False

    def clean(self):
        data = super(ListAdminForm, self).clean()

        if data['tagged_delivery']:
            if not data['tagkey']:
                self.add_error('tagkey', 'Must be enabled when tagged delivery is enabled')
            if not data['taglistsource']:
                self.add_error('taglistsource', 'Must be enabled when tagged delivery is enabled')
        else:
            if data['tagkey']:
                self.add_error('tagkey', "Can't be enabled unless tagged delivery is enabled")
            if data['taglistsource']:
                self.add_error('taglistsource', "Can't be enabled unless tagged delivery is enabled")
        return data


class ListAliasForm(forms.ModelForm):
    class Meta:
        model = ListAlias
        fields = ['alias', ]

    def clean_alias(self):
        # List aliases can't be duplicated, but this could be a value used
        # by another *list*...
        (local, domain) = self.cleaned_data['alias'].lower().split('@')

        if List.objects.filter(name=local, domain__name=domain).exists():
            raise forms.ValidationError("A list with address {0} already exists".format(self.cleaned_data['alias'].lower()))

        return self.cleaned_data['alias'].lower()


class ListAliasInline(admin.TabularInline):
    model = ListAlias
    extra = 2
    form = ListAliasForm


class ListAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'domain', 'subscription_policy', 'moderation_level', 'archivedat')
    list_filter = ('domain__name', 'archivedat', 'moderation_level', 'subscription_policy')
    autocomplete_fields = ['moderators', ]
    form = ListAdminForm
    inlines = [ListAliasInline, ]


class DomainAdminForm(forms.ModelForm):
    class Meta:
        model = Domain
        exclude = ()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['archiveservers'].widget.can_add_related = False
        self.fields['archiveservers'].widget.can_change_related = False


class DomainAdmin(admin.ModelAdmin):
    form = DomainAdminForm


class SubscriberAdmin(admin.ModelAdmin):
    # This admin is only needed for autocompplete, so make sure nobody can access it.
    # This also makes it disappear from the list of available tables.
    def has_module_permission(self, request):
        return False

    search_fields = ('user__first_name', 'user__last_name', 'user__email', )


class GlobalBlocklistAdmin(admin.ModelAdmin):
    list_display = ['address', 'usecount', 'lastused']
    fields = ['address', 'exclude']


admin.site.register(Subscriber, SubscriberAdmin)
admin.site.register(Domain, DomainAdmin)
admin.site.register(List, ListAdmin)
admin.site.register(ListGroup)
admin.site.register(ArchiveServer)
admin.site.register(ListWhitelist, ListWhitelistAdmin)
admin.site.register(GlobalWhitelist)
admin.site.register(GlobalBlocklist, GlobalBlocklistAdmin)
admin.site.register(NeverBlockRegexp)
admin.site.register(MessageidModerationList)
admin.site.register(GlobalModerationList)
admin.site.register(CannedRejectResponse)
admin.site.register(CannedUnsubscriptionNotice)
