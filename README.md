# PGLister

PGLister is a mailing lists system developed and used by the PostgreSQL community.

# Description

PGLister is a django-based mailing list application, updated to work with recent improvements in email technology and spam filtering.

More information here: https://wiki.postgresql.org/wiki/PGLister_Announce

# Install

The procedure is described [here](INSTALL.md).

There is an ansible install script [here](https://gitlab.com/cmatte/ansible-pglister).

# Tools

There are some available tools in the `tools/` folder:
- `list_admin_out.py`: list contents of admin out queue, decoding fields for debugging purposes.
- `list_moderation_queue.py`: list contents of moderatino queue, decoding field for debugging purposes.
- `sync_moderators_list_membership.sql`: synchronize the membership of a list to be all the moderators of all lists in a specific domain.
Additionnaly, `bin/pglistercli.py` can be used to unregister users from the command line.

# Other documentation

- [Moderation](https://wiki.postgresql.org/wiki/PGLister:_How_to_Moderate)
