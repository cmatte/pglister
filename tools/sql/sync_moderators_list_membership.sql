--
-- This script is intended to run to synchronize the membership of a list to be all the moderators
-- of all lists in a specific domain.
-- It should be run through psql, and executed like:
--  psql pglister -f sync_moderators_list_membership.sql  -v domain=lists.testlist.org -v listaddress=moderators@lists.testlist.org


WITH emails AS (
  SELECT DISTINCT sa.id as said, sa.email
  FROM mailinglist_moderators m
  INNER JOIN lists_list l ON l.id=m.listid
  INNER JOIN lists_subscriberaddress sa ON sa.email=m.email
  WHERE l.domain_id=(SELECT id FROM lists_domain d WHERE d.name=:'domain')
),
list AS (
  SELECT id AS listid  FROM mailinglists WHERE address=:'listaddress'
),
data AS (
  SELECT said, listid FROM list CROSS JOIN emails
),
ins AS (
   INSERT INTO lists_listsubscription (subscription_confirmed, nomail, list_id, subscriber_id)
   SELECT true, false, listid, said
   FROM data
   WHERE NOT EXISTS (SELECT 1 FROM lists_listsubscription ls WHERE ls.list_id=data.listid AND ls.subscriber_id=said)
   RETURNING 'ins' AS what, subscriber_id
),
del AS (
   DELETE FROM lists_listsubscription WHERE list_id=(SELECT listid FROM list) AND NOT EXISTS (SELECT 1 FROM data WHERE said=subscriber_id)
   RETURNING 'del', subscriber_id
),
out AS (
   SELECT * FROM ins
   UNION ALL
   SELECT * FROM del
)
SELECT :'listaddress' AS list, what, email
FROM out INNER JOIN lists_subscriberaddress sa ON sa.id=out.subscriber_id;

