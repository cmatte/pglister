Double-click on the message in open letter view.  Then go to the View menu
and click on Message Source.  The full headers are now visible and you
should find the 'List-Unsubscribe' header.  Copy and paste the URL shown
into your browser and follow the instructions provided.
