#!/bin/bash

# Script to import subscribers and email archives from mailman to pglister.
# Lists must already be created in pglister.
# Here is where to find some options in mailman administrative interface for each list:
# - Subscription policy:
#     Privacy options -> "What steps are required for subscription?"
# - Members view membership:
#     Privacy options -> "Who can view subscription list?"
# - Moderation level:
#     Privacy options -> Sender filter -> "By default, should new list member postings be moderated?"

ORIGIN="" # server hosting mailman
TARGET="" # server hosting pglister
PUBLIC_LISTS="" # space-separate list of public lists to import
PRIVATE_LISTS="" # space-separate list of private lists to import
DOMAIN="" # fqdn used for lists
DJANGO_USER="list"
PYTHON_VENV_PGLISTER="/srv/pglister/local/web/pglister/bin/python"
PYTHON_VENV_PGARCHIVES="/srv/pgarchives/local/django/bin/python"
PYTHON_VENV_PGARCHIVES_PRIVATE="/srv/pgarchives-private/local/django/bin/python"
PGARCHIVES_PATH="/srv/pgarchives/local/"
PGARCHIVES_PRIVATE_PATH="/srv/pgarchives-private/local/"
PGLISTER_PATH="/srv/pglister/local/"

function import_list() {
    listname_full="$1"
    listname=$(echo "$listname_full" | tr '[:upper:]' '[:lower:]')
    echo "Loading subscribers from $listname_full"
    ssh $ORIGIN /var/lib/mailman/bin/list_members -f -n -o "$listname"_nomail.txt "$listname_full"
    ssh $ORIGIN /var/lib/mailman/bin/list_members -f -o "$listname"_mail.txt "$listname_full"
    scp $ORIGIN:"$listname"_nomail.txt $TARGET:lists/
    scp $ORIGIN:"$listname"_mail.txt $TARGET:lists/
    ssh $TARGET sudo -u $DJANGO_USER $PYTHON_VENV_PGLISTER $PGLISTER_PATH/bin/load_subscribers.py -l "$listname"@$DOMAIN -a -f lists/"$listname"_nomail.txt --nomail
    ssh $TARGET sudo -u $DJANGO_USER $PYTHON_VENV_PGLISTER $PGLISTER_PATH/bin/load_subscribers.py -l "$listname"@$DOMAIN -a -f lists/"$listname"_mail.txt
    echo "Loading archives from $listname_full"
    scp $ORIGIN:/var/lib/mailman/archives/private/"$listname".mbox/"$listname".mbox $TARGET:lists/
    if [ "$2" == "public" ]
    then
        ssh $TARGET sudo -u $DJANGO_USER $PYTHON_VENV_PGARCHIVES $PGARCHIVES_PATH/loader/load_message.py -m lists/"$listname".mbox -l "$listname"
    else
        ssh $TARGET sudo -u $DJANGO_USER $PYTHON_VENV_PGARCHIVES_PRIVATE $PGARCHIVES_PRIVATE_PATH/loader/load_message.py -m lists/"$listname".mbox -l "$listname"
    fi
}

for i in $PUBLIC_LISTS
do
    import_list "$i" public
done

for i in $PRIVATE_LISTS
do
    import_list "$i" private
done
