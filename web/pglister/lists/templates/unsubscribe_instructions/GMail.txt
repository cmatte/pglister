You must use the regular web interface for GMail (the mobile GMail client
does not allow you to view headers).  Open an email from the mailing list.
On the far right is a down-arrow button (to the right of the reply button).
Click on the drop-down array and then click on 'Show original'.  Scroll
down until you see the "List-Unsubscribe" header.  Copy and paste the URL
shown into your browser and follow the instructions provided.
