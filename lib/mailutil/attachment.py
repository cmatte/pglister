def _get_one_attachment_info(msg):
    try:
        name = msg.get_filename()
        if not name:
            if 'Content-description' in msg:
                name = msg['Content-Description']

        yield {
            'name': name or '*unknown*',
            'type': msg.get_content_type(),
            'size': len(msg.get_payload(decode=True)),
        }
    except AssertionError:
        # Just ignore
        pass


def get_attachment_info(msg):
    if msg.get_content_type() == 'multipart/mixed' or msg.get_content_type() == 'multipart/signed':
        # Multipart, so worth scanning into this
        if not msg.is_multipart():
            # Should never happen, so ignore this part
            return
        for p in msg.get_payload():
            if p.get_params() is None:
                continue
            yield from get_attachment_info(p)
    elif msg.get_content_type() == 'multipart/alternative':
        # Alternative shouldn't be attachments
        return
    elif msg.is_multipart():
        # Weird other kinds of multipart, ignore
        return
    else:
        # Not multipart, so this is potentially the actual attachment
        if msg.get_content_type() in ('application/pgp-signature', 'application/pkcs7-signature', 'application/x-pkcs7-signature'):
            return
        if msg.get_content_type() != 'text/plain':
            yield from _get_one_attachment_info(msg)
            return
        # text/plain is considered an attachment if it has a name
        if not msg.get_params():
            return
        for k, v in msg.get_params():
            if k == 'name' and v != '':
                yield from _get_one_attachment_info(msg)
                return

        # OK, what if it actually claims to be an attachment?
        if 'Content-Disposition' in msg and msg['Content-Disposition'].startswith('attachment'):
            yield from _get_one_attachment_info(msg)
            return
