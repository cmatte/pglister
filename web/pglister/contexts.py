from django.conf import settings


# Template context processor to add information about organization name
# and motto defined in settings.py
def PGListerContextProcessor(request):
    return {
        'organization_info': settings.ORGANIZATION_INFO,
    }
