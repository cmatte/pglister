Your request to unsubscribe from $name has been received, but
must be confirmed before it will be completed. To confirm
unsubscribing from $name, please click the link below. If you
do not want to unsubscribe, you can ignore this email.

Unsubscribe: ${webroot}/unsubscribe-confirm/${token}/
